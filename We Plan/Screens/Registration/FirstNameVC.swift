//
//  FirstNameVC.swift
//  We Plan
//
//  Created by apple on 9/24/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class FirstNameVC: UIViewController {

  lazy var Label1:UILabel = {
    let label = UILabel()
    label.text = "What’s Your Name?"
    label.font = AppFont.getFont(type: .noramlBold, size: 18)
    label.textAlignment = .center
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    return label
  }()
  
  lazy var Label2:UILabel = {
    let label = UILabel()
    label.text = "Using your Real name makes it easier for people to recoginzie you."
    label.font = AppFont.getFont(type: .normalRegular, size: 12)
    label.textAlignment = .center
    label.numberOfLines = 2
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    return label
  }()
  
  lazy var firstNameContainerView:UIView = {
    var view = UIView()
    view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
    view.layer.cornerRadius = 8
    return view
  }()
  
  lazy var firstNameTextField:UITextField = {
    let textField = UITextField()
    textField.placeholder = "First Name"
    textField.font = AppFont.getFont(type: .normalRegular, size: 12)
    textField.spellCheckingType = .no
    textField.autocorrectionType = .no
    textField.tintColor = UIColor.WePaln.primaryColor
    return textField
  }()
  
  lazy var lastNameContainerView:UIView = {
    var view = UIView()
    view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
    view.layer.cornerRadius = 8
    return view
  }()
  
  lazy var lastNameTextField:UITextField = {
    let textField = UITextField()
    textField.placeholder = "Last Name"
    textField.font = AppFont.getFont(type: .normalRegular, size: 12)
    textField.spellCheckingType = .no
    textField.autocorrectionType = .no
    textField.tintColor = UIColor.WePaln.primaryColor
    return textField
  }()
  
  lazy var nextButton:UIButton = {
    let button = UIButton()
    button.setTitle("Next", for: .normal)
    button.backgroundColor = UIColor.WePaln.primaryColor
    button.titleLabel?.font = AppFont.getFont(type: .noramlBold, size: 12)
    button.layer.cornerRadius = 8
    return button
  }()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setupViews()
    }
    
  func setupViews() {
    view.addSubviews([Label1,firstNameContainerView,firstNameTextField,lastNameContainerView,lastNameTextField,Label2,nextButton])
    
    Label1.snp.makeConstraints { (make) in
      make.top.equalToSuperview().offset(UIScreen.main.bounds.height * 0.21)
      make.leading.equalTo(83)
    }
    
    firstNameContainerView.snp.makeConstraints { (make) in
      make.leading.equalTo(16)
      make.width.equalTo(158)
      make.height.equalTo(40)
      make.top.equalTo(Label1.snp.bottom).offset(30)
    }
    
    firstNameTextField.snp.makeConstraints { (make) in
      make.leading.equalTo(firstNameContainerView.snp.leading).offset(16)
      make.top.equalTo(firstNameContainerView.snp.top).offset(9)
    }
    
    lastNameContainerView.snp.makeConstraints { (make) in
      make.leading.equalTo(firstNameContainerView.snp.trailing).offset(20)
      make.width.equalTo(158)
      make.height.equalTo(40)
      make.top.equalTo(Label1.snp.bottom).offset(30)
    }
    
    lastNameTextField.snp.makeConstraints { (make) in
      make.leading.equalTo(lastNameContainerView.snp.leading).offset(16)
      make.top.equalTo(lastNameContainerView.snp.top).offset(9)
    }
    
    Label2.snp.makeConstraints { (make) in
      make.leading.equalTo(24)
      make.trailing.equalTo(-24)
      make.top.equalTo(firstNameContainerView.snp.bottom).offset(30)
      make.height.equalTo(36)
    }
    
    nextButton.snp.makeConstraints { (make) in
      make.leading.equalTo(16)
      make.width.equalTo(336)
      make.height.equalTo(44)
      make.top.equalTo(Label2.snp.bottom).offset(52)
    }
    
  }

}
