//
//  FontUtils.swift
//  We Plan
//
//  Created by apple on 9/23/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Localize_Swift
import DeviceKit

enum AppFontTypes: String {
  case englishBold = "Montserrat-Bold"
  case englishRegular = "Montserrat-Regular"
  case englishMedium = "Montserrat-Medium"
  case englishItalic = "Lato-Italic"
  case englishTitleFontRegular = "Optima-Regular"
  case englishTitleFontBold = "Optima-Bold"
  
  case arabicBold = "SegoeUI-Bold"
  case arabicRegular = "SegoeUI"
  case arabicTitleFont = "GEDinarOne-Medium"
}

/**
 Contains all fonts states that is used in the App.
 */

enum AppFontStates {
  case noramlBold
  case normalRegular
  case normalMedium
  case normalItalic
  case titleBold
  case titleRegular
}

class AppFont {
  /**
   Prints all fonts names.
   */
  static func printFonts() {
    let fontFamilyName = UIFont.familyNames
    for familyName in fontFamilyName {
      print("------------------------------")
      print("Font Family Name = [\(familyName)]")
      let names = UIFont.fontNames(forFamilyName: familyName)
      print("Font Names = [\(names)]")
    }
  }
  
  /**
   Returns an instance of UIFont with a specific name and size.
   - Parameter type: Font state (Normal, Bold, ...).
   - Parameter size: Font size.
   - Returns: An instance of UIFont.
   */
  static func getFont(type: AppFontStates, size: CGFloat) -> UIFont {
    var fontType:AppFontTypes!
    switch type {
    case .normalRegular:
      fontType = Localize.currentLanguage() == "en" ? .englishRegular : .arabicRegular
    case .normalItalic:
      fontType = Localize.currentLanguage() == "en" ? .englishItalic : .arabicRegular
    case .noramlBold:
      fontType = Localize.currentLanguage() == "en" ? .englishBold : .arabicBold
    case .titleRegular:
      fontType = Localize.currentLanguage() == "en" ? .englishTitleFontRegular : .arabicTitleFont
    case .titleBold:
      fontType = Localize.currentLanguage() == "en" ? .englishTitleFontBold : .arabicTitleFont
    case.normalMedium:
      fontType = Localize.currentLanguage() == "en" ? .englishMedium : .arabicRegular
    }
    
    
    return getFontSize(name: fontType.rawValue, fontSize: size) //UIFont(name: fontType.rawValue, size: size)!
  }
  
  /**
   Returns an instance of UIFont with a specific name and size according to the current device.
   - Parameter name: Font name.
   - Parameter size: Font size.
   - Returns: An instance of UIFont.
   */
  static func getFontSize(name:String, fontSize:CGFloat) -> UIFont {
    let groupOfAllowedDevices: [Device] = [.iPhone6sPlus,.iPhone7Plus,.iPhone8Plus,.iPhoneX,.iPhoneXR,.iPhoneXS,.iPhoneXSMax,.simulator(.iPhone6Plus),.simulator(.iPhone7Plus),.simulator(.iPhone8Plus),.simulator(.iPhoneX),.simulator(.iPhoneXR),.simulator(.iPhoneXS)]
    let device = Device.current
    
    if device.isOneOf(groupOfAllowedDevices) {
      return UIFont(name: name, size: fontSize + 2)!
      
    } else {
      return UIFont(name: name, size: fontSize)!
    }
  }
  
  
}
