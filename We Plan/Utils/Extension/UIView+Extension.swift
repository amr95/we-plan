//
//  UIView+Extension.swift
//  We Plan
//
//  Created by apple on 9/23/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//
import UIKit

extension UIView {
  
  /// EZSE: add multiple subviews
  public func addSubviews(_ views: [UIView]) {
    views.forEach { [weak self] eachView in
      self?.addSubview(eachView)
    }
  }
  
  
}
